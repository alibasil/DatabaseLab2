/* Lab02 Queries */
/* 1) */ SELECT INITCAP(fname) || ' , ' || INITCAP (lname) AS FULLNAME FROM author WHERE INSTR(lname,'IN') != 0 ORDER BY (fname) ASC;
/* 2) */ SELECT SUBSTR(title,1,10) FROM books WHERE category LIKE 'C%' AND INSTR(category,'COMPUTER') = 0;
/* 3) */ SELECT (title) || ' , ' || (cost * 1.15) from books WHERE (cost * 1.15) < 30;
/* 4) */ SELECT (title) || ' , ' || pubdate FROM books WHERE category NOT IN ('COMPUTER','CHILDREN','FITNESS') AND SUBSTR(pubdate,LENGTH(pubdate)) != 5;
/* 6) */ SELECT (title) || ' , ' || ((cost - nvl(discount,0)) * 1.15) FROM books;